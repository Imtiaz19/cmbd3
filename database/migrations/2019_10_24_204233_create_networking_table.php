<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNetworkingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('networking', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('server_id')->unsigned();
            $table->string('name', 32)->nullable();
            $table->string('mac', 60)->nullable();
            $table->ipaddress('ipadd')->nullable();
            $table->string('hostname', 128)->nullable();
            $table->string('netmask', 128)->nullable();
            $table->timestamps();
            $table->foreign('server_id')->references('id')->on('servers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('networking');
    }
}
