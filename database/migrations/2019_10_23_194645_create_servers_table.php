<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hostname', 128);
            $table->ipAddress('ipaddr')->nullable();
            $table->smallInteger('product_id')->unsigned();
            $table->smallInteger('environment_id')->unsigned();
            $table->smallInteger('purpose_id')->unsigned();
            $table->smallInteger('hardware_id')->unsigned();
            $table->smallInteger('location_id')->unsigned();
            $table->string('serial', 128)->nullable();
            $table->text('notes')->nullable();
            $table->string('os', 128)->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('environment_id')->references('id')->on('environments');
            $table->foreign('purpose_id')->references('id')->on('purposes');
            $table->foreign('hardware_id')->references('id')->on('hardware');
            $table->foreign('location_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
