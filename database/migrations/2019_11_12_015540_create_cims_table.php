<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cims', function (Blueprint $table) {
            $table->bigInteger('server_id')->unsigned();
            $table->index('server_id');
            $table->unique('server_id');
            $table->ipAddress('ipaddr')->nullable();
            $table->string('description')->nullable();
            $table->string('password', 64)->nullable();
            $table->string('details')->nullable();
            $table->foreign('server_id')->references('id')->on('servers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cims');
    }
}
