<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Environment;
use Faker\Generator as Faker;

$factory->define(App\Models\Environment::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->word,
    ];
});
