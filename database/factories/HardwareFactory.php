<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Hardware;
use Faker\Generator as Faker;

$factory->define(App\Models\Hardware::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->word,
    ];
});
