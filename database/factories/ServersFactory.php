<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Server;;
use Faker\Generator as Faker;

$factory->define(App\Models\Server::class, function (Faker $faker) {
    return [
        'hostname' => $faker->domainName,
        'ipaddr' => $faker->ipv4,
        'product_id' => $faker->numberBetween(1, 3),
        'environment_id' => $faker->numberBetween(1, 3),
        'purpose_id' => $faker->numberBetween(1, 3),
        'hardware_id' => $faker->numberBetween(1, 3),
        'location_id' => $faker->numberBetween(1, 3),
        'serial' => $faker->uuid,
        'notes' => $faker->dateTime(),
        'os' => $faker->chrome,
    ];
});
