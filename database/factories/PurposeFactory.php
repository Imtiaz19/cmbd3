<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Purpose;
use Faker\Generator as Faker;

$factory->define(App\Models\Purpose::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->word,
    ];
});
