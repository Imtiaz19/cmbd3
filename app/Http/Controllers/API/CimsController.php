<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cims;
use App\Models\Server;
use App\Http\Requests\CimcRequest;


class CimsController extends Controller
{
    public function index() {
        return Cims::all();
    }

    public function store(CimcRequest $request) {
        return Cims::create($request->all());
    }

    public function show($id)
    {
        $server = Server::find($id);
                
        if (empty($server)) {
            $server = Server::where('hostname', $id)->first();
            $cimc = Cims::where('server_id', $id)->first();
        }

        if (empty($server)) {
            $server = Server::where('ipaddr', $id)->first();
        }
        
        if (empty($server)) {
            $server = Server::where('serial', $id)->first();
        }

        if ($server) {
            $cimc = Cims::where('server_id', $server->id)->first();

            return response()->json(array(
                'server' => $server,
                'cimc' => $cimc,
            ));
        }
        else {
            return response()->json("None for $id found");
        }
    }

    public function update(Request $request, $id)
    {

        $server = Server::find($id);
        

        if (empty($server)) {
            $server = Server::where('hostname', $id)->first();
        }

        if (empty($server)) {
            $server = Server::where('ipaddr', $id)->first();
        }
        
        if (empty($server)) {
            $server = Server::where('serial', $id)->first();
        }

        if ($server) {
            $cimc = Cims::where('server_id', $id)->first();
            $cimc->update($request->all());

            return response()->json(array(
                'server' => $server,
                'cimc' => $cimc,
            ));
        }
        else {
            return response()->json("None for $id found");
        }
    }

    public function destroy($id) {
        return response()->json("Method is not supported. Please refer to MonOps API documentation at https://JozuNp88J9V8zN33");
    }
}
