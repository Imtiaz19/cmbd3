<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Server;
use App\Http\Requests\ServersRequest;
use App\Models\ServerDetails;

class ServersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Server::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServersRequest $request)
    {
        return Server::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $server = Server::find($id);

        if (empty($server)) {
            $server = Server::where('hostname', $id)->first();
        }

        if (empty($server)) {
            $server = Server::where('ipaddr', $id)->first();
        }
        
        if (empty($server)) {
            $server = Server::where('serial', $id)->first();
        }

        if ($server) {
            return $server;
        }
        else {
            return response()->json("None found");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $server = Server::findOrFail($id);
        $server->update($request->all());

        return $server;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $server = Server::findOrFail($id);
        $server->delete();

        return 204;
    }

    public function showdetails($id)
    {
        $server = ServerDetails::find($id);

        if ($server) {
            return $server;
        }
        else {
            return response()->json("None found");
        }
    }

    public function updatedetails(Request $request, $id) {

        $details = ServerDetails::updateOrCreate(['server_id' => $id], ['data' => $request->data]);

        //$details->data = json_decode($request->data, true);

        $details->data = $request->data;
        //$details->save()

        if($details->save()) {
            $server = Server::find($id);
            return response()->json("Added or update data for $server->hostname with data: $details->data");
        } else {
            return response()->json($details->data);
        }
        
    }
}