<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductsController extends Controller
{
    public function index() {
        $products = Product::paginate(10);
        return view('products.index', compact('products'));
    }

    public function create() {
        return view('products.create');
    }

    public function store(Request $request) {

        $validatedData = $request->validate([
            'name' => 'required|max:60|unique:products,name',
            'description' => 'required|max:255',
            'team' => 'required|max:60',
            'email' => 'required|max:255|email',
        ]);
        $product = Product::create($validatedData);
   
        return redirect('/products')->with('success', 'Product is successfully saved');
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);

        return view('products.edit', compact('product'));
    }

    public function update(Request $request, $id)
{
        $validatedData = $request->validate([
            'name' => 'required|max:60|unique:products,name,'.$id,
            'description' => 'required|max:255',
            'team' => 'required|max:60',
            'email' => 'required|email|max:255',
        ]);
        Product::whereId($id)->update($validatedData);

        return redirect('/products')->with('success', 'Product has been successfully updated');
}

    public function destroy($id) {
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect('/products')->with('success', 'Product is successfully deleted');
    }
}
