<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Purpose;

class PurposesController extends Controller
{
    public function index() {
        $purposes = Purpose::paginate(10);
        return view('purpose.index', compact('purposes'));
    }

    public function create() {
        return view('purpose.create');
    }

    public function store(Request $request) {

        $validatedData = $request->validate([
            'name' => 'required|max:60|unique:purposes,name',
            'description' => 'required|max:255',
        ]);
        $purposes = Purpose::create($validatedData);
   
        return redirect('/purposes')->with('success', 'Purpose has been successfully saved');
    }

    public function edit($id)
    {
        $purpose = Purpose::findOrFail($id);

        return view('purpose.edit', compact('purpose'));
    }

    public function update(Request $request, $id)
{
        $validatedData = $request->validate([
            'name' => 'required|max:60|unique:purposes,name,'.$id,
            'description' => 'required|max:255',
        ]);
        Purpose::whereId($id)->update($validatedData);

        return redirect('/purposes')->with('success', 'Purpose has been successfully updated');
}

    public function destroy($id) {
        $purposes = Purpose::findOrFail($id);
        $purposes->delete();

        return redirect('/purposes')->with('success', 'Purpose has geen successfully deleted');
    }
}
