<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Server;
use App\Models\Product;
use App\Models\Environment;
use App\Models\Purpose;
use App\Models\Location;
use App\Models\Hardware;
use App\Models\ServerDetails;
use App\Models\Cims;

class ServersController extends Controller
{
    public function index() {

        $servers = Server::paginate(10);
        $products = Product::all();
        $environments = Environment::all();
        $purposes = Purpose::all();
        $locations = Location::all();
        $hardwares = Hardware::all();

        return view('servers.index')
            ->with('products', $products)
            ->with('servers', $servers)
            ->with('environments', $environments)
            ->with('purposes', $purposes)
            ->with('locations', $locations)
            ->with('hardwares', $hardwares);
    }

    public function create() {
        
        $servers = Server::all();
        $products = Product::all();
        $environments = Environment::all();
        $purposes = Purpose::all();
        $locations = Location::all();
        $hardware = Hardware::all();

        return view('servers.create')
            ->with('products', $products)
            ->with('servers', $servers)
            ->with('environments', $environments)
            ->with('purposes', $purposes)
            ->with('locations', $locations)
            ->with('hardwares', $hardware);
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
            'hostname' => 'required|max:128|unique:servers,hostname',
            'ipaddr' => 'ip',
            'product_id' => 'required|exists:products,id',
            'environment_id' => 'required|exists:environments,id',
            'purpose_id' => 'required|exists:purposes,id',
            'hardware_id' => 'required|exists:hardware,id',
            'location_id' => 'required|exists:locations,id',
            'serial' => 'required|max:128',
            'notes' => 'max:256',
            'os' => 'max:128'
        ]);
        $server = Server::create($validatedData);
   
        return redirect('/servers')->with('success', "Server $server->hostname has been successfully added");
    }

    public function edit($id) {

        $server = Server::findOrFail($id);
        $products = Product::all();
        $environments = Environment::all();
        $purposes = Purpose::all();
        $locations = Location::all();
        $hardware = Hardware::all();

        return view('servers.edit')
            ->with('server', $server)
            ->with('products', $products)
            ->with('environments', $environments)
            ->with('purposes', $purposes)
            ->with('locations', $locations)
            ->with('hardwares', $hardware);
    }

    public function update(Request $request, $id) {
        $validatedData = $request->validate([
            'hostname' => 'required|max:128|unique:servers,hostname,'.$id,
            'ipaddr' => 'ip',
            'product_id' => 'required|exists:products,id',
            'environment_id' => 'required|exists:environments,id',
            'purpose_id' => 'required|exists:purposes,id',
            'hardware_id' => 'required|exists:hardware,id',
            'location_id' => 'required|exists:locations,id',
            'serial' => 'required|max:128',
            'notes' => 'max:256',
            'os' => 'max:128'
        ]);
        Server::whereId($id)->update($validatedData);
        $server = Server::find($id);

        return redirect('/servers')->with('success', "Server $server->hostname has been successfully updated.");
    }

    public function destroy($id) {
        $server = Server::findOrFail($id);
        $server->delete();

        return redirect('/servers')->with('success', "Server $server->hostname has been successfully deleted");
    }

    public function showdetails($id) {
        $server = Server::where('id', $id)->first();
        $detailsdata = ServerDetails::where('server_id', $id)->first();
        $cimc = Cims::where('server_id', $id)->first();

        if ($detailsdata) {
        $details = collect(json_decode($detailsdata->data, true));
        }

        if ($cimc) {
            $cimcdetails = collect(json_decode($cimc->details, true));
        }

        return view('servers.details')->with(compact('details', 'server', 'cimc', 'cimcdetails'));
    }

    public function updatedetails(Request $request, $id) {

        $validatedData = $request->validate([
            'data' => 'required'
        ]);

        $server = Server::findOrFail($id);

        if ($server) {
            $server = ServerDetails::whereId($id)->update($validatedData);
        } else {
            $server = ServerDetails::create($validatedData);
        }

        return redirect('/servers')->with('success', "Data has been successfully updated.");
    }

}
