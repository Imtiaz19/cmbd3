<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hardware;

class HardwareController extends Controller
{
    public function index() {
        $hardwares = Hardware::paginate(10);
        return view('hardware.index', compact('hardwares'));
    }

    public function create() {
        return view('hardware.create');
    }

    public function store(Request $request) {

        $validatedData = $request->validate([
            'name' => 'required|max:60|unique:hardware,name',
            'description' => 'required|max:255',
            'units' => 'numeric',
        ]);
        $hardware = Hardware::create($validatedData);
   
        return redirect('/hardware')->with('success', 'Hardware is successfully saved');
    }

    public function edit($id)
    {
        $hardware = Hardware::findOrFail($id);

        return view('hardware.edit', compact('hardware'));
    }

    public function update(Request $request, $id)
{
        $validatedData = $request->validate([
            'name' => 'required|max:60|unique:hardware,name,'.$id,
            'description' => 'required|max:255',
            'units' => 'numeric',
        ]);
        Hardware::whereId($id)->update($validatedData);

        return redirect('/hardware')->with('success', 'Hardware has been successfully updated');
}

    public function destroy($id) {
        $hardware = Hardware::findOrFail($id);
        $hardware->delete();

        return redirect('/hardware')->with('success', 'Hardware is successfully deleted');
    }
}
