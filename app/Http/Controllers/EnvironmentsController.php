<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Environment;

class EnvironmentsController extends Controller
{
    public function index() {
        $environments = Environment::paginate(10);
        return view('environment.index', compact('environments'));
    }

    public function create() {
        return view('environment.create');
    }

    public function store(Request $request) {

        $validatedData = $request->validate([
            'name' => 'required|max:60|unique:environments,name',
            'description' => 'required|max:255',
        ]);
        $environments = Environment::create($validatedData);
   
        return redirect('/environments')->with('success', 'environment has been successfully saved');
    }

    public function edit($id)
    {
        $environment = Environment::findOrFail($id);

        return view('environment.edit', compact('environment'));
    }

    public function update(Request $request, $id)
{
        $validatedData = $request->validate([
            'name' => 'required|max:60|unique:environments,name,'.$id,
            'description' => 'required|max:255',
        ]);
        Environment::whereId($id)->update($validatedData);

        return redirect('/environments')->with('success', 'environment has been successfully updated');
}

    public function destroy($id) {
        $environment = Environment::findOrFail($id);
        $environment->delete();

        return redirect('/environments')->with('success', 'environment has been successfully deleted');
    }
}
