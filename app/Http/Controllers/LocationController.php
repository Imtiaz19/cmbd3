<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Location;

class LocationController extends Controller
{
    public function index() {
        $locations = Location::paginate(10);
        return view('location.index', compact('locations'));
    }

    public function create() {
        return view('location.create');
    }

    public function store(Request $request) {

        $validatedData = $request->validate([
            'name' => 'required|max:60|unique:locations,name',
            'region' => 'required|max:20',
        ]);
        $location = Location::create($validatedData);
   
        return redirect('/location')->with('success', 'Location has been successfully saved');
    }

    public function edit($id)
    {
        $location = Location::findOrFail($id);

        return view('location.edit', compact('location'));
    }

    public function update(Request $request, $id)
{
        $validatedData = $request->validate([
            'name' => 'required|max:60|unique:locations,name,'.$id,
            'region' => 'required|max:20',
        ]);
        Location::whereId($id)->update($validatedData);

        return redirect('/location')->with('success', 'Location has been successfully updated');
}

    public function destroy($id) {
        $location = Location::findOrFail($id);
        $location->delete();

        return redirect('/location')->with('success', 'Location has been deleted');
    }
}
