<?php

namespace App\Http\Requests;

use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;

use Illuminate\Foundation\Http\FormRequest;

class ServersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hostname' => 'required|max:128|unique:servers,hostname',
            'ipaddr' => 'ip',
            'product_id' => 'required|exists:products,id',
            'environment_id' => 'required|exists:environments,id',
            'purpose_id' => 'required|exists:purposes,id',
            'hardware_id' => 'required|exists:hardware,id',
            'location_id' => 'required|exists:locations,id',
            'serial' => 'required|max:128',
            'notes' => 'max:256',
            'os' => 'max:128'
        ];
    }

    public function messages()
    {
        return [
            'hostname.required' => 'Hostname field is required',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
