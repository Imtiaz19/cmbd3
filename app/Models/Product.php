<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'description',
        'team',
        'email',
    ];

    public function server()
    {
        return $this->hasMany(App\Models\Server::class);
    }
}
