<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purpose extends Model
{
    protected $fillable = [
        'name',
        'description',
    ];

    public function server()
    {
        return $this->hasMany(App\Models\Server::class);
    }
}
