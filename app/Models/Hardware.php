<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hardware extends Model
{
    protected $fillable = [
        'name',
        'description',
        'units'
    ];

    public function server()
    {
        return $this->hasMany(App\Models\Server::class);
    }
}
