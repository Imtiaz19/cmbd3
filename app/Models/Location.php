<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'name',
        'region',
    ];

    public function server()
    {
        return $this->hasMany(App\Models\Server::class);
    }
}
