<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cims extends Model
{
    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = [
        'server_id',
        'ipaddr',
        'description',
        'password',
        'details'
    ];

    protected $casts = [
        'server_id' => 'int',
        'data' => 'string'
    ];

    public function server()
    {
        return $this->hasMany(App\Models\Server::class);
    }
}
