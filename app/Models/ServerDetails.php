<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServerDetails extends Model
{

    public $incrementing = false;

    protected $casts = [
        'server_id' => 'int',
        'data' => 'string'
    ];

    protected $fillable = [
        'server_id',
        'data'
    ];

    public function server()
    {
        return $this->hasMany(App\Models\Server::class);
    }

}
