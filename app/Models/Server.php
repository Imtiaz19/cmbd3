<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $fillable = [
        'hostname',
        'ipaddr',
        'product_id',
        'environment_id',
        'purpose_id',
        'hardware_id',
        'location_id',
        'serial',
        'notes',
        'os'
    ];

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }

    public function environment() {
        return $this->belongsTo('App\Models\Environment');
    }

    public function purpose() {
        return $this->belongsTo('App\Models\Purpose');
    }

    public function hardware() {
        return $this->belongsTo('App\Models\Hardware');
    }

    public function location() {
        return $this->belongsTo('App\Models\Location');
    }

    public function details() {
        return $this->belongsTo('App\Models\ServerDetails');
    }
}
