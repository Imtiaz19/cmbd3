@extends('layouts.master')

@section('content')

@section('pagename')
    List all locations/regions
@endsection
@section('breadcrumb', 'Location')
<div class="uper">
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br/>
    @endif

    <a href="{{ route('location.create') }}" class="btn btn-primary" style="margin-bottom:10px">Create new location</a>

    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List all locations/regions</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Location Name</td>
                                <td>Region</td>
                                <td colspan="2">Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($locations as $location)
                                <tr>
                                    <td>{{$location->id}}</td>
                                    <td><a href="{{ route('location.edit', $location->id)}}">{{$location->name}}</a>
                                    </td>
                                    <td>{{$location->region}}</td>
                                    <td><a href="{{ route('location.edit', $location->id)}}" class="btn btn-primary">Edit</a>
                                    </td>
                                    <td>
                                        <form action="{{ route('location.destroy', $location->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! $locations->links() !!}

    <div>
@endsection
