@extends('layouts.master')
@section('content')

@section('pagename')
    Create a new location enrty
@endsection
@section('breadcrumb', 'Location')
<div class="card uper">
    <div class="card-header">
        Add new location
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('location.store') }}">
            <div class="form-group">
                @csrf
                <label for="name">Location Name:</label>
                <input type="text" class="form-control" name="name" value="{{ old('name') }}"/>
            </div>
            <div class="form-group">
                <label for="region">Region Description:</label>
                <input type="text" class="form-control" name="region" value="{{ old('region') }}"/>
            </div>

            <button type="submit" class="btn btn-primary">Create new location</button>
        </form>
    </div>
</div>
@endsection
