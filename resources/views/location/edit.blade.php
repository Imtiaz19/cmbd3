@extends('layouts.master')

@section('content')

@section('pagename')
    Edit Location: {{ $location->name }}
@endsection
@section('breadcrumb', 'Location')
<div class="card uper">
    <div class="card-header">
        Edit Location: <strong>{{ $location->name }}</strong>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('location.update', $location->id) }}">
            <div class="form-group">
                @csrf
                @method('PATCH')
                <label for="name">Location Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $location->name }}"/>
            </div>
            <div class="form-group">
                <label for="price">Region:</label>
                <input type="text" class="form-control" name="region" value="{{ $location->region }}"/>
            </div>

            <button type="submit" class="btn btn-primary">Update Location</button>
        </form>
    </div>
</div>
@endsection
