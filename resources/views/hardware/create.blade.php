@extends('layouts.master')

@section('content')

@section('pagename')
    Create a hardware record
@endsection
@section('breadcrumb', 'Hardware')
<div class="card uper">
    <div class="card-header">
        Add new hardware
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('hardware.store') }}">
            <div class="form-group">
                @csrf
                <label for="name">Hardware Name:</label>
                <input type="text" class="form-control" name="name" value="{{ old('name') }}"/>
            </div>
            <div class="form-group">
                <label for="description">Hardware Description:</label>
                <input type="text" class="form-control" name="description" value="{{ old('description') }}"/>
            </div>
            <div class="form-group">
                <label for="units">Hardware Units:</label>
                <input type="text" class="form-control" name="units" value="{{ old('units') }}" value="0"/>
            </div>
            <button type="submit" class="btn btn-primary">Create Hardware</button>
        </form>
    </div>
</div>
@endsection
