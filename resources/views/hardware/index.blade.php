@extends('layouts.master')

@section('content')

@section('pagename')
    List all hardware details
@endsection
@section('breadcrumb', 'Hardware')
<div class="uper">
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br/>
    @endif

    <a href="{{ route('hardware.create') }}" class="btn btn-primary" style="margin-bottom:10px">Create hardware</a>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List all hardware</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Name</td>
                                <td>Description</td>
                                <td>Units</td>
                                <td colspan="2">Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($hardwares as $hardware)
                                <tr>
                                    <td>{{$hardware->id}}</td>
                                    <td><a href="{{ route('hardware.edit', $hardware->id)}}">{{$hardware->name}}</a>
                                    </td>
                                    <td>{{$hardware->description}}</td>
                                    <td>{{$hardware->units}}</td>
                                    <td><a href="{{ route('hardware.edit', $hardware->id)}}" class="btn btn-primary">Edit</a>
                                    </td>
                                    <td>
                                        <form action="{{ route('hardware.destroy', $hardware->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {!! $hardwares->links() !!}
    <div>
@endsection
