@extends('layouts.master')

@section('content')

@section('pagename')
    Edit Hardware: {{ $hardware->name }}
@endsection
@section('breadcrumb', 'Hardware')
<div class="card uper">
    <div class="card-header">
        Edit Hardware: <strong>{{ $hardware->name }}</strong>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('hardware.update', $hardware->id) }}">
            <div class="form-group">
                @csrf
                @method('PATCH')
                <label for="name">Hardware Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $hardware->name }}"/>
            </div>
            <div class="form-group">
                <label for="price">Hardware Description:</label>
                <input type="text" class="form-control" name="description" value="{{ $hardware->description }}"/>
            </div>
            <div class="form-group">
                <label for="price">Units:</label>
                <input type="text" class="form-control" name="team" value="{{ $hardware->units }}"/>
            </div>
            <button type="submit" class="btn btn-primary">Update Hardware</button>
        </form>
    </div>
</div>
@endsection
