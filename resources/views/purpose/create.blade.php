@extends('layouts.master')

@section('content')
@section('pagename')
    Add Purpose
@endsection
@section('breadcrumb', 'Purpose')
<div class="card uper">
    <div class="card-header">
        Add Purpose
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('purposes.store') }}">
            <div class="form-group">
                @csrf
                <label for="name">Purpose Name:</label>
                <input type="text" class="form-control" name="name" value="{{ old('name') }}"/>
            </div>
            <div class="form-group">
                <label for="description">Purpose Description:</label>
                <input type="text" class="form-control" name="description" value="{{ old('description') }}"/>
            </div>
            <button type="submit" class="btn btn-primary">Create Purpose</button>
        </form>
    </div>
</div>
@endsection
