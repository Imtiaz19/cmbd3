@extends('layouts.master')

@section('content')
@section('pagename')
    List all purposes
@endsection
@section('breadcrumb', 'Purpose')
<div class="uper">
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br/>
    @endif

    <a href="{{ route('purposes.create') }}" class="btn btn-primary" style="margin-bottom:10px">Create purpose</a>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List all purposes</h3>
                    </div>
                    <div class="card-body">

                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Name</td>
                                <td>Description</td>
                                <td colspan="2">Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($purposes as $purpose)
                                <tr>
                                    <td>{{$purpose->id}}</td>
                                    <td><a href="{{ route('purposes.edit', $purpose->id)}}">{{$purpose->name}}</a></td>
                                    <td>{{$purpose->description}}</td>
                                    <td><a href="{{ route('purposes.edit', $purpose->id)}}"
                                           class="btn btn-primary">Edit</a></td>
                                    <td>
                                        <form action="{{ route('purposes.destroy', $purpose->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! $purposes->links() !!}

    <div>
@endsection
