@extends('layouts.master')

@section('content')

@section('pagename')
    Edit Purpose: {{ $purpose->name }}
@endsection
@section('breadcrumb', 'Purpose')
<div class="card uper">
    <div class="card-header">
        Edit Purpose: <strong>{{ $purpose->name }}</strong>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('purposes.update', $purpose->id) }}">
            <div class="form-group">
                @csrf
                @method('PATCH')
                <label for="name">Purpose Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $purpose->name }}"/>
            </div>
            <div class="form-group">
                <label for="price">Product Description:</label>
                <input type="text" class="form-control" name="description" value="{{ $purpose->description }}"/>
            </div>

            <button type="submit" class="btn btn-primary">Update {{ $purpose->name }}</button>
        </form>
    </div>
</div>
@endsection
