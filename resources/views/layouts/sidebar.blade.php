<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">

        <span class="brand-text font-weight-light">CMDB</span>
    </a>

    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
    {{--  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
       <div class="image">
         <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
       </div>
       <div class="info">
         <a href="#" class="d-block">Alexander Pierce</a>
       </div>
     </div>
--}}
    <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-header">CMDB
                    <a href="/servers/create" style="float: right;">
                        <ion-icon name="add-circle-outline" class="nav-icon" size="small"></ion-icon>
                    </a>

                </li>
                <li class="nav-item ">
                    <a class="nav-link @if (\Request::is('servers')) active @endif" href="/servers">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Servers

                        </p>
                    </a>
                </li>

            </ul>
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-header">SETTINGS</li>
                <li class="nav-item ">
                    <a class="nav-link @if (\Request::is('products')) active @endif" href="/products">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Products
                        </p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link @if (\Request::is('purposes')) active @endif" href="/purposes">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Purposes
                        </p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link @if (\Request::is('environments')) active @endif" href="/environments">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Environments
                        </p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link @if (\Request::is('hardware')) active @endif" href="/hardware">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Hardware
                        </p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link @if (\Request::is('location')) active @endif" href="/location">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Location
                        </p>
                    </a>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
