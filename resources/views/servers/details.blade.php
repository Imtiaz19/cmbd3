@extends('layouts.master')

@section('content')

@section('pagename')
    Server Information: {{ $server->hostname }}
@endsection
@section('breadcrumb', 'Server')
<div class="row col-2">
    <a href="{{ route('servers.edit', $server->id)}}" class="btn btn-primary">Edit</a>

    <form action="{{ route('servers.destroy', $server->id)}}" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" type="submit">Delete</button>
    </form>
</div>

<div class="row">


    <div class="col-6">

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th scope="col">Hostname</th>
                <th scope="col">{{ $server->hostname }}</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>ID</td>
                <td>{{ $server->id }}</td>
            </tr>

            <tr>
                <td>IP Address</td>
                <td>{{ $server->ipaddr }}</td>
            </tr>

            <tr>
                <td>Product</td>
                <td>{{ $server->product->name }}</td>
            </tr>

            <tr>
                <td>Environment</td>
                <td>{{ $server->environment->name }}</td>
            </tr>

            <tr>
                <td>Purpose</td>
                <td>{{ $server->purpose->name }}</td>
            </tr>

            <tr>
                <td>Hardware</td>
                <td>{{ $server->hardware->name }}</td>
            </tr>

            <tr>
                <td>Location</td>
                <td>{{ $server->location->name }}</td>
            </tr>

            <tr>
                <td>Operation System</td>
                <td>{{ $server->os }}</td>
            </tr>

            <tr>
                <td>Serial #</td>
                <td>{{ $server->serial }}</td>
            </tr>

            <tr>
                <td>Notes</td>
                <td>{{ $server->notes }}</td>
            </tr>

            </tbody>
        </table>


        @if ($cimc)
            <h2> CIMC Data </h2>
            <table class="table table-bordered table-striped">
                <tbody>
                <tr>
                    <td>CIMC IP Address</td>
                    <td>{{ $cimc->ipaddr }}</td>
                </tr>
                <tr>
                    <td>CIMC Description</td>
                    <td>{{ $cimc->description }}</td>
                </tr>
                <tr>
                    <td>CIMC Password</td>
                    <td>{{ Str::limit($cimc->password, $limit = 3, $end = '********') }} <i> (first 3 chars only)</i>
                    </td>
                </tr>
                <tr>
                    <td>CIMC Details</td>
                    <td>{{ $cimcdetails }}</td>
                </tr>
                </tbody>
            </table>
        @endif
    </div>

    @if (isset($details))
        <div class="col-6">
            <table class="table table-sm table-bordered table-striped">
                <tbody class=".table-striped">
                <tr class="table-primary">
                    <th colspan="2">System Information</th>
                </tr>
                <tr>
                    <td>SysInfo.Version</td>
                    <td>{{ $details['sysinfo']['version'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>SysInfo.Timestamp</td>
                    <td>{{ $details['sysinfo']['timestamp'] ?? "Unknown" }}</td>
                </tr>

                <tr class="table-primary">
                    <th colspan="2">Node Information</th>
                </tr>
                <tr>
                    <td>Node.Hostname</td>
                    <td>{{ $details['node']['hostname'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Node.MachineId</td>
                    <td>{{ $details['node']['machineid'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Node.Hypervisor</td>
                    <td>{{ $details['node']['hypervisor'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Node.Timezone</td>
                    <td>{{ $details['node']['timezone'] ?? "Unknown" }}</td>
                </tr>

                <tr class="table-primary">
                    <th colspan="2">Operation System Information</th>
                </tr>
                <tr>
                    <td>OS.Name</td>
                    <td>{{ $details['os']['name'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>OS.Vendor</td>
                    <td>{{ $details['os']['vendor'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>OS.Version</td>
                    <td>{{ $details['os']['version'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>OS.Release</td>
                    <td>{{ $details['os']['release'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>OS.Architecture</td>
                    <td>{{ $details['os']['architecture'] ?? "Unknown" }}</td>
                </tr>

                <tr class="table-primary">
                    <th colspan="2">Kernel Information</th>
                </tr>
                <tr>
                    <td>Kernel.Release</td>
                    <td>{{ $details['kernel']['release'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Kernel.Version</td>
                    <td>{{ $details['kernel']['version'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Kernel.Architecture</td>
                    <td>{{ $details['kernel']['architecture'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Kernel.Release</td>
                    <td>{{ $details['kernel']['release'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Kernel.Architecture</td>
                    <td>{{ $details['kernel']['architecture'] ?? "Unknown" }}</td>
                </tr>

                <!-- product -->

                <tr class="table-primary">
                    <th colspan="2">H/W Product Information</th>
                </tr>
                <tr>
                    <td>Product.Name</td>
                    <td>{{ $details['product']['name'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Product.vendor</td>
                    <td>{{ $details['product']['vendor'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Product.version</td>
                    <td>{{ $details['product']['version'] ?? "Unknown" }}</td>
                </tr>


                <!-- board -->

                <tr class="table-primary">
                    <th colspan="2">Board Information</th>
                </tr>
                <tr>
                    <td>Board.Name</td>
                    <td>{{ $details['board']['chassis'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Board.vendor</td>
                    <td>{{ $details['board']['vendor'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Board.version</td>
                    <td>{{ $details['board']['version'] ?? "Unknown" }}</td>
                </tr>

                <!-- chassis -->

                <tr class="table-primary">
                    <th colspan="2">Chassis Information</th>
                </tr>
                <tr>
                    <td>Chassis.Type</td>
                    <td>{{ $details['chassis']['type'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Chassis.vendor</td>
                    <td>{{ $details['chassis']['vendor'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Chassis.version</td>
                    <td>{{ $details['chassis']['version'] ?? "Unknown" }}</td>
                </tr>

                <!-- bios -->

                <tr class="table-primary">
                    <th colspan="2">Bios Information</th>
                </tr>
                <tr>
                    <td>Bios.Vendor</td>
                    <td>{{ $details['bios']['vendor'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Bios.Version</td>
                    <td>{{ $details['bios']['version'] ?? "Unknown" }}</td>
                </tr>
                </tr>

                <!-- cpu -->
                <tr class="table-primary">
                    <th colspan="2">CPU Information</th>
                </tr>
                <tr>
                    <td>CPU.Vendor</td>
                    <td>{{ $details['cpu']['vendor'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>CPU.Model</td>
                    <td>{{ $details['cpu']['model'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>CPU.Speed</td>
                    <td>{{ $details['cpu']['speed'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>CPU.Cache</td>
                    <td>{{ $details['cpu']['cache'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>CPU.Threds</td>
                    <td>{{ $details['cpu']['threads'] ?? "Unknown" }}</td>
                </tr>
                </tr>

                <!-- memory  -->
                <tr class="table-primary">
                    <th colspan="2">Memory Information</th>
                </tr>
                <tr>
                    <td>Memory.Type</td>
                    <td>{{ $details['memory']['type'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Memory.Size</td>
                    <td>{{ $details['memory']['size'] ?? "Unknown" }}</td>
                </tr>
                </tr>

                <!-- memory  -->
                <tr class="table-primary">
                    <th colspan="2">Memory Information</th>
                </tr>
                <tr>
                    <td>Memory.Type</td>
                    <td>{{ $details['memory']['type'] ?? "Unknown" }}</td>
                </tr>
                <tr>
                    <td>Memory.Size</td>
                    <td>{{ $details['memory']['size'] ?? "Unknown" }}</td>
                </tr>
                </tr>

                <!-- storage -->

                <tr class="table-primary">
                    <th colspan="2">Storage Information</th>
                </tr>

                @foreach ($details['storage'] as $storage)
                    <tr>
                        <td>Storage.Name</td>
                        <td>{{ $storage['name'] ?? "Unknown" }}</td>
                    </tr>
                    <tr>
                        <td>Storage.Driver</td>
                        <td>{{ $storage['driver'] ?? "Unknown" }}</td>
                    </tr>
                    <tr>
                        <td>Storage.Size</td>
                        <td>{{ $storage['size'] ?? "Unknown" }} GB</td>
                    </tr>
                    @endforeach
                    </tr>

                    <!-- networking -->

                    <tr class="table-primary">
                        <th colspan="2">Network Information</th>
                    </tr>

                    @foreach ($details['network'] as $network)
                        <tr>
                            <td>Network.Name</td>
                            <td>{{ $network['name'] ?? "Unknown" }}</td>
                        </tr>
                        <tr>
                            <td>Network.Driver</td>
                            <td>{{ $network['driver'] ?? "Unknown" }}</td>
                        </tr>
                        <tr>
                            <td>Network.Size</td>
                            <td>{{ $network['macaddress'] ?? "Unknown" }}</td>
                        </tr>
                        @endforeach
                        </tr>

                </tbody>
            </table>
        </div>
    @endif


</div>


@endsection
