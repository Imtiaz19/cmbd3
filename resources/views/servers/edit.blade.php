@extends('layouts.master')

@section('content')

@section('pagename')
    Edit Server: {{ $server->hostname }}
@endsection
@section('breadcrumb', 'Server')
<div class="card uper">
    <div class="card-header">
        Edit Server: <strong> {{ $server->hostname }} </strong>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('servers.update', $server->id) }}">
            <div class="form-group">
                @csrf
                @method('PATCH')
                <label for="hostname">Server Name:</label>
                <input type="text" class="form-control" name="hostname"
                       value="{{ old('hostname', $server->hostname) }}"/>
            </div>

            <div class="form-group">
                <label for="ipaddr">IP Address:</label>
                <input type="text" class="form-control" name="ipaddr" value="{{ old('ipaddr', $server->ipaddr) }}"/>
            </div>

            <div class="form-group">
                <label for="product_id">Product (current: {{ $server->product->name }}):</label>
                <select name="product_id" class="form-control">
                    @foreach($products as $product)
                        <option
                            value="{{ $product->id }}" {{ $product->id === $server->product->id ? 'selected="selected"' : '' }}>
                            {{ $product->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="environment_id">Environment (current: {{ $server->environment->name }}):</label>
                <select class="form-control" name="environment_id">
                    @foreach($environments as $environment)
                        <option
                            value="{{$environment->id}}" {{ $environment->id === $server->environment->id ? 'selected="selected"' : '' }}>
                            {{ $environment->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="purpose_id">Purpose (current: {{ $server->purpose->name }}):</label>
                <select class="form-control" name="purpose_id">
                    @foreach($purposes as $purpose)
                        <option
                            value="{{$purpose->id}}" {{ $purpose->id === $server->purpose->id ? 'selected="selected"' : '' }}>
                            {{ $purpose->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="hardware_id">Hardware Type (current: {{ $server->hardware->name }}):</label>
                <select class="form-control" name="hardware_id">
                    @foreach($hardwares as $hardware)
                        <option
                            value="{{$hardware->id}}" {{ $hardware->id === $server->hardware->id ? 'selected="selected"' : '' }}>
                            {{ $hardware->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="location_id">Location (current: {{ $server->location->name}}
                    / {{ $server->location->region }}):</label>
                <select class="form-control" name="location_id">
                    @foreach($locations as $location)
                        <option
                            value="{{$location->id}}" {{ $location->id === $server->location->id ? 'selected="selected"' : '' }}>
                            {{ $location->name }} / {{ $location->region }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="os">Operation system:</label>
                <input type="text" class="form-control" name="os" value="{{ old('os', $server->os) }}"/>
            </div>

            <div class="form-group">
                <label for="serial">Serial #:</label>
                <input type="text" class="form-control" name="serial" value="{{ old('serial', $server->serial) }}"/>
            </div>

            <div class="form-group">
                <label for="notes">Notes:</label>
                <textarea rows="5" class="form-control" cols="50" name="notes"
                          id="notes">{{ old('notes', $server->notes) }}</textarea>
            </div>
            <div class="form-group">
                <label for="created_at">Created on:</label>
                <strong>{{ old('created_at', $server->created_at) }}</strong
            </div>
            <div class="form-group">
                <label for="updated_at">Updated on:</label>
                <strong>{{ old('updated_at', $server->updated_at) }}</strong>
            </div>

            <button type="submit" class="btn btn-primary">Edit Server</button>
        </form>

        <form action="{{ route('servers.destroy', $server->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger float-right" type="submit">Delete</button>
        </form>
    </div>
</div>
@endsection
