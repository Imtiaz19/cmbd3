@extends('layouts.master')

@section('content')

@section('pagename')
    List Servers
@endsection
@section('breadcrumb', 'Server')
<div class="uper">
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br/>
    @endif

    <form action="/search" method="POST" role="search">
        {{ csrf_field() }}
        <div class="input-group pb-3">
            <input type="text" class="form-control" name="q"
                   placeholder="Search servers by hostname or Serial #"> <span class="input-group-btn">
            <button type="submit" class="btn btn-block btn-default">
             <i class="fas fa-search"></i>
            </button>
        </span>
        </div>
    </form>

    <a href="{{ route('servers.create') }}" class="btn btn-primary" style="margin-bottom:10px">Create Server</a>

    <div class="container pb-4">
        @if(isset($details))
            <p> The Search results for your query <b> {{ $query }} </b> are :</p>
            <h2>Sample User details</h2>
            <table class="table table-striped table-bordered table-sm">
                <thead>
                <tr>
                    <th>Hostname</th>
                    <th>Serial</th>
                </tr>
                </thead>
                <tbody>
                @foreach($details as $server)
                    <tr>
                        <td>{{$server->hostname}}</td>
                        <td>{{$server->serial}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>


    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">DataTable with minimal features & hover style</h3>
                    </div>
                    <div class="card-body">

                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Hostname</th>
                                <th>IP Address</th>
                                <th>Product</th>
                                <th>Environment</th>
                                <th>Purpose</th>
                                <th>Hardware</th>
                                <th>Location</th>
                                <th>Serial</th>
                                <th colspan="2">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($servers as $server)
                                <tr>
                                    <td>{{$server->id}}</td>
                                    <td><a href="{{ route('details', $server->id)}}">{{$server->hostname}}</a></td>
                                    <td>{{ $server->ipaddr }}</td>
                                    <td>{{ $server->product->name }}</td>
                                    <td>{{ $server->environment->name }}</td>
                                    <td>{{ $server->purpose->name }}</td>
                                    <td>{{ $server->hardware->name }}</td>
                                    <td>{{ $server->location->name }}</td>
                                    <td>{{ $server->serial }}</td>
                                    <td>
                                        <a href="{{ route('details', $server->id)}}"
                                           class="btn btn-block btn-info btn-sm">Details</a>
                                        <a href="{{ route('servers.edit', $server->id)}}"
                                           class="btn btn-block btn-primary btn-sm">Edit</a>
                                        <form action="{{ route('servers.destroy', $server->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button style="margin-top: .5rem;" class="btn btn-block btn-danger btn-sm"
                                                    type="submit">Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Hostname</th>
                                <th>IP Address</th>
                                <th>Product</th>
                                <th>Environment</th>
                                <th>Purpose</th>
                                <th>Hardware</th>
                                <th>Location</th>
                                <th>Serial</th>
                                <th colspan="2">Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="align-bottom">
        {!! $servers->links() !!}
    </div>
    <div>
@endsection
@push('scripts')

    @push('scripts')
