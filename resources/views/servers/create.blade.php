@extends('layouts.master')
@section('content')
@section('pagename', 'Add new server')
@section('breadcrumb', 'Server')

<div class="card uper">
    <div class="card-header">
        Add new server
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('servers.store') }}">
            <div class="form-group">
                @csrf
                <label for="hostname">Server Name:</label>
                <input type="text" class="form-control" name="hostname" value="{{ old('hostname') }}"/>
            </div>

            <div class="form-group">
                <label for="ipaddr">IP Address:</label>
                <input type="text" class="form-control" name="ipaddr" value="{{ old('ipaddr') }}"/>
            </div>

            <div class="form-group">
                <label for="product_id">Product:</label>
                <select name="product_id" class="form-control">
                    @foreach($products as $product)
                        <option value="{{$product->id}}">{{$product->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="environment_id">Environment:</label>
                <select class="form-control" name="environment_id">
                    @foreach($environments as $environment)
                        <option value="{{$environment->id}}">{{$environment->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="purpose_id">Purpose:</label>
                <select class="form-control" name="purpose_id">
                    @foreach($purposes as $purpose)
                        <option value="{{$purpose->id}}">{{$purpose->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="hardware_id">Hardware:</label>
                <select name="hardware_id" class="form-control">
                    @foreach($hardwares as $hardware)
                        <option value="{{$hardware->id}}">{{$hardware->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="location_id">Location:</label>
                <select name="location_id" class="form-control">
                    @foreach($locations as $location)
                        <option value="{{$location->id}}">{{$location->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="os">Operation system:</label>
                <input type="text" class="form-control" name="os" value="{{ old('os') }}"/>
            </div>

            <div class="form-group">
                <label for="serial">Serial #:</label>
                <input type="text" class="form-control" name="serial" value="{{ old('serial') }}"/>
            </div>

            <button type="submit" class="btn btn-primary">Add Server</button>
        </form>
    </div>
</div>
@endsection
