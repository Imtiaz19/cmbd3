@extends('layouts.master')

@section('content')

@section('pagename')
    Search
@endsection
@section('breadcrumb', 'Server')
<div class="uper">
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br/>
    @endif

    <form action="/search" method="POST" role="search" style="margin-bottom:10px">
        {{ csrf_field() }}
        <div class="input-group">
            <input type="text" class="form-control" name="q"
                   placeholder="Search servers by hostname or Serial #"> <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <i class="fas fa-search"></i>
            </button>
        </span>
        </div>
    </form>


    @if(isset($details))

        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-body">

                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Hostname</td>
                                    <td>IP Address</td>
                                    <td>Product</td>
                                    <td>Environment</td>
                                    <td>Purpose</td>
                                    <td>Hardware</td>
                                    <td>Location</td>
                                    <td>Serial</td>
                                    <td colspan="2">Action</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($details as $server)
                                    <tr>
                                        <td>{{$server->id}}</td>
                                        <td><a href="{{ route('servers.edit', $server->id)}}">{{$server->hostname}}</a>
                                        </td>
                                        <td>{{$server->ipaddr}}</td>
                                        <td>{{$server->product->name}}</td>
                                        <td>{{$server->environment->name}}</td>
                                        <td>{{$server->purpose->name}}</td>
                                        <td>{{$server->hardware->name}}</td>
                                        <td>{{$server->location->name}}</td>
                                        <td>{{ $server->serial }}</td>
                                        <td><a href="{{ route('servers.edit', $server->id)}}"
                                               class="btn btn-block btn-primary btn-sm">Edit</a>

                                            <form action="{{ route('servers.destroy', $server->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-block btn-danger btn-sm"
                                                        style="margin-top: .5rem;" type="submit">Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</div>
@endif
<a href="{{ route('servers.create') }}" class="btn btn-primary">Create Server</a>
<div>
@endsection
