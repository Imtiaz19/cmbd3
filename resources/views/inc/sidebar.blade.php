<nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
      <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>CMDB</span>
          <a class="d-flex align-items-center text-muted" href="/servers/create">
            <span data-feather="plus-circle"></span>
          </a>
        </h6>

        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link @if (\Request::is('servers')) active @endif" href="/servers">
              <i data-feather="server"></i>
              Servers 
            </a>
          </li>

        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Settings</span>
          <!-- <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="plus-circle"></span>
          </a> -->
        </h6>
        <ul class="nav flex-column mb-2">
        <li class="nav-item">
            <a class="nav-link @if (\Request::is('products')) active @endif" href="/products">
              <span data-feather="box"></span>
              Products
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if (\Request::is('purposes')) active @endif" href="/purposes">
              <span data-feather="map-pin"></span>
              Purpose
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if (\Request::is('environments')) active @endif" href="/environments">
              <span data-feather="grid"></span>
              Environments
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if (\Request::is('hardware')) active @endif" href="/hardware">
              <span data-feather="layers"></span>
              Hardware
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if (\Request::is('location')) active @endif" href="/location">
              <span data-feather="map-pin"></span>
              Location
            </a>
          </li>

        </ul>
      </div>
    </nav>