@extends('layouts.master')

@section('content')

@section('pagename')
    Edit Environment: {{ $environment->name }}
@endsection
@section('breadcrumb', 'environment')
<div class="card uper">
    <div class="card-header">
        Edit Environment: <strong>{{ $environment->name }}</strong>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('environments.update', $environment->id) }}">
            <div class="form-group">
                @csrf
                @method('PATCH')
                <label for="name">Environment Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $environment->name }}"/>
            </div>
            <div class="form-group">
                <label for="price">Environment Description:</label>
                <input type="text" class="form-control" name="description" value="{{ $environment->description }}"/>
            </div>

            <button type="submit" class="btn btn-primary">Update Environment</button>
        </form>
    </div>
</div>
@endsection
