@extends('layouts.master')

@section('content')

@section('pagename')
    List all environments
@endsection
@section('breadcrumb', 'Products')


<div class="uper pb-3">
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br/>
    @endif

    <a href="{{ route('environments.create') }}" class="btn btn-primary" style="margin-bottom:10px">Create
        Environment </a>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List all environments</h3>
                    </div>
                    <div class="card-body">

                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Name</td>
                                <td>Description</td>
                                <td colspan="2">Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($environments as $environment)
                                <tr>
                                    <td>{{$environment->id}}</td>
                                    <td>
                                        <a href="{{ route('environments.edit', $environment->id)}}">{{$environment->name}}</a>
                                    </td>
                                    <td>{{$environment->description}}</td>
                                    <td><a href="{{ route('environments.edit', $environment->id)}}"
                                           class="btn btn-primary">Edit</a></td>
                                    <td>
                                        <form action="{{ route('environments.destroy', $environment->id)}}"
                                              method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! $environments->links() !!}

    <div>
@endsection
