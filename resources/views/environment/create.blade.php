@extends('layouts.master')

@section('content')

@section('pagename')
    Add Environment
@endsection
@section('breadcrumb', 'environment')
<div class="card uper">
    <div class="card-header">
        Add Environment
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('environments.store') }}">
            <div class="form-group">
                @csrf
                <label for="name">Environment Name:</label>
                <input type="text" class="form-control" name="name" value="{{ old('name') }}"/>
            </div>
            <div class="form-group">
                <label for="description">Environment Description:</label>
                <input type="text" class="form-control" name="description" value="{{ old('description') }}"/>
            </div>

            <button type="submit" class="btn btn-primary">Create Environment</button>
        </form>
    </div>
</div>
@endsection
