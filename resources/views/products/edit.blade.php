@extends('layouts.master')

@section('content')

@section('pagename')
    Edit Product: {{ $product->name }}
@endsection
@section('breadcrumb', 'Products')
<div class="card uper">
    <div class="card-header">
        Edit Product: <strong>{{ $product->name }}</strong>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('products.update', $product->id) }}">
            <div class="form-group">
                @csrf
                @method('PATCH')
                <label for="name">Product Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $product->name }}"/>
            </div>
            <div class="form-group">
                <label for="price">Product Description:</label>
                <input type="text" class="form-control" name="description" value="{{ $product->description }}"/>
            </div>
            <div class="form-group">
                <label for="price">Owner Team:</label>
                <input type="text" class="form-control" name="team" value="{{ $product->team }}"/>
            </div>
            <div class="form-group">
                <label for="quantity">Alerts E-mail:</label>
                <input type="text" class="form-control" name="email" value="{{ $product->email }}"/>
            </div>
            <button type="submit" class="btn btn-primary">Update Product</button>
        </form>
    </div>
</div>
@endsection
