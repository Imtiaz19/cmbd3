@extends('layouts.master')

@section('content')

@section('pagename')
    List all products
@endsection
@section('breadcrumb', 'Products')

<div class="uper">
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br/>
    @endif

    <a href="{{ route('products.create') }}" class="btn btn-primary" style="margin-bottom:10px">Create product</a>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List all products</h3>
                    </div>
                    <div class="card-body">

                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Name</td>
                                <td>Description</td>
                                <td>Owner Team</td>
                                <td>Team Email</td>
                                <td colspan="2">Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{$product->id}}</td>
                                    <td><a href="{{ route('products.edit', $product->id)}}">{{$product->name}}</a></td>
                                    <td>{{$product->description}}</td>
                                    <td>{{$product->team}}</td>
                                    <td>{{$product->email}}</td>
                                    <td><a href="{{ route('products.edit', $product->id)}}"
                                           class="btn btn-primary">Edit</a></td>
                                    <td>
                                        <form action="{{ route('products.destroy', $product->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! $products->links() !!}
    <div>
@endsection
