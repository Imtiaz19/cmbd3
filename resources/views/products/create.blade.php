@extends('layouts.master')

@section('content')

@section('pagename')
    Add a new product
@endsection

<div class="card uper">
    <div class="card-header">
        Add Product
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('products.store') }}">
            <div class="form-group">
                @csrf
                <label for="name">Product Name:</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}"/>
            </div>
            <div class="form-group">
                <label for="description">Product Description:</label>
                <input type="text" class="form-control" name="description" id="description"
                       value="{{ old('description') }}"/>
            </div>
            <div class="form-group">
                <label for="team">Owner Team:</label>
                <input type="text" class="form-control" name="team" value="{{ old('team') }}"/>
            </div>
            <div class="form-group">
                <label for="email">Alerts E-mail:</label>
                <input type="text" class="form-control" name="email" value="{{ old('email') }}"/>
            </div>
            <button type="submit" class="btn btn-primary">Create Product</button>
        </form>
    </div>
</div>
@endsection
