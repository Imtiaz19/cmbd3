@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <a href="/servers" class="btn btn-block bg-gradient-primary">List servers</a></br>
            <a href="/products" class="btn btn-block bg-gradient-secondary">List products</a></br>

        </div>
    </div>

@endsection
