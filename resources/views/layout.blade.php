<!doctype html>
<html>
<head>
    @include('inc.head')
</head>
<body>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/">CMDB</a>
</nav>

<div class="container-fluid">
  <div class="row">

    @include('inc.sidebar')

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@yield('pagename', 'CMDB')</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group mr-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
          </div>
          <!-- <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar"></span>
            This week
          </button> -->
        </div>
      </div>

          @yield('content')

        </main>
      </div>
    </div>

    <!-- <footer class="row fixed-bottom">
        @include('inc.footer')
    </footer> -->

<script src="{{ mix('js/app.js') }}"></script>
<script src="/js/feather.min.js"></script>
<script>
      feather.replace()
    </script>

</body>
</html>