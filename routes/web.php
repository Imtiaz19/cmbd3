<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Server;
use Illuminate\Support\Facades\Request;

Route::get('/', 'HomeController@index');

Route::resource('servers', 'ServersController');

Route::resource('products', 'ProductsController');
Route::resource('purposes', 'PurposesController');
Route::resource('environments', 'EnvironmentsController');
Route::resource('hardware', 'HardwareController');
Route::resource('location', 'LocationController');
Route::get('details/{id}', 'ServersController@showdetails')->name('details');;

Route::any('/search',function(){
    $q = Request::get ( 'q' );
    $server = Server::where('hostname','LIKE','%'.$q.'%')->orWhere('serial','LIKE','%'.$q.'%')->get();
    if(count($server) > 0)
        return view('servers.search')->withDetails($server)->withQuery ( $q );
    else return view ('servers.search')->withMessage('No Servers found. Try to search again !');
});