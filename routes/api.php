<?php

use Illuminate\Http\Request;

use App\Models\Server;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('servers', 'API\ServersController');
Route::resource('hardware', 'API\HardwareController');
Route::resource('products', 'API\ProductController');
Route::resource('product', 'API\ProductController');
Route::resource('purpose', 'API\PurposeController');
Route::resource('cimc', 'API\CimsController');
Route::get('details/{id}', 'API\ServersController@showdetails');
Route::match(['put', 'post'], 'details/{id}', 'API\ServersController@updatedetails');

//Route::get('servers/{domain}', 'API\ServersController@domain');